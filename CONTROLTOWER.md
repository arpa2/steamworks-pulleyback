# Control Towers using Pulley

> *The use of Pulley works well with an idea that we call the
> "control tower" of our InternetWide Architecture.*

We are devising a hosting infrastructure with a strong focus
on privacy and security.  At the heart of this lies the idea
of domains as a starting point for online presence, and the
possible reliance on hosting providers to populate domains
with (1) identity information and (2) services.


## Open Containers and their structure

Our concrete hosting infrastructure is a "mkroot" package that
loads our innovative software into standardised Open Containers.
These containers use Linux' network namespaces in a flexible
manner, and allow partial overlap between containers on one or
a few name spaces.

An example could be:

  * A webserver serves domains and content from a static
    configuration, and may proxy to backends with dynamic
    plugins on some paths.

    Websites are shielded from unauthorised changes because
    the webserver cannot change its configuration or page
    content.  Attacks over HTTP will therefore not work.

  * Administration access is required for some packages,
    but may be constrained to portions of the webserver
    that require login, like with HTTP-SASL over HTTPS.
    Access control can also be used on such paths.  It
    is even possible to run this kind of activity on
    different hosts, not the public-facing website.

  * A separate upload interface manages the web content and
    supports website uploads through SFTP.  This means that
    the web content is mounted in a writeable form.  This
    content does not execute here, so it is less dangerous.
    Furthermore, authenticated access protects the content
    from abuse by others.

  * Configuration information is mostly generated.  While
    the hosting provider's domain `${IWO_DOMAIN_ISP}` is
    a special case that can serve as default content, all
    customer domains are setup in a standard manner using
    Pulley Backends driven from Pulley Scripts.  Central
    configuration in LDAP is managed from a separate
    interface, and automatically lands in configuration
    files.  After writing out new configuration data, the
    Web server is signaled to reload it.  **This is the
    main task of the Control Tower.**

  * Certificates can also be handled outside of the web
    server, and merely presented.  One way of doing this
    is via the usual files in the container mount space,
    another might be through a TLS Pool that could be
    run in a separate container, and therefore shield the
    private keys from the web server.  **Certificates can
    be obtained by the Control Tower.**

In short, our container design allows us to pick and choose
the bits that we want to reveal in various places, and the
access rights granted on each.  Public-facing services can
be constrained to readonly behaviour and possibly not even
see private keys on which they operate.


## Control Tower reign over containers

The Control Tower is a container that serves in the backend,
and may interact with a container over its own `ctl0` network
interface.  If your server listens for control traffic, it
should do that on the `ctl0` interface, and not anywhere else.
This can work for `rndc` in BIND9.

Likewise, when commands need to be run to control a daemon
in a container, it may share the PID space of the container
and the reload trigger might be run from the Control Tower.
This can work for `postfix reload` or `apachectl reload`.

Finally, when a UNIX domain socket needs to be opened to
control a deamon then a first question is whether it could
be a `ctl0` network listener, and if not, the mount namespace
could be shared with the Control Tower to make it access
that particular socket.  Alternatively, a forwarder may listen
on `ctl0` only and connect to the UNIX domain socket when it
is being contacted.

There is support in POSIX for passing file descriptors between
containers.  This may offer an alternative approach for passing
control out over the trusted `ctl0` interface.


## Control Tower tasks

The Control Tower retrieves generally formulated
configuration information (such as `john` is a
user of domain `example.org`) and translating that into
settings for a variety of services.  There are flexible
mechanisms for producing configuration files and triggering
daemons through some of the mechanisms described above.

The Control Tower may have to combine information
from various sources.  As a simple example, it is not
enough if `john` wants to be a member of the `cooks` group,
nor is it enough if `cooks` invites `john`.  Both need to
be active before `john` becomes a group member of `cooks`.

The Control Tower may not find all desired information in
one place, because not all of it will be local.
Considering the market potential for a
[hosting split into identity and service](http://internetwide.org/blog/2014/11/19/back-to-hosting.html)
it is likely that a Service Provider obtains identity
information from a variety of Identity Providers.
This allows each to be the best they can be, as long
as they exchange the configuration data needed for a
collaboration.  It is up to the Control Tower to bring
`example.org` and `example.com` together one the same
webserver, even if their domains are registered with
different identity providers.

The Control Tower ensures coordinated behaviour for sets
of services.  Perhaps one container handles the DNS
mention of a webserver, another container offers the web
service and yet another permits uploading of web content
to the rightful owners.  The containers do not need to
know about each other, because the overview is held by
the Control Tower.  This also means that they do not
communicate, and an attack on one container does not
really help to get in on another.

The Control Tower is dynamic.  As soon as things change,
an update to the configuration is ordered, and passed
around as configurations for the dependent containers.
It subscribes to configuration changes and actively
pushes it into containers.  Containers merely serve
their current configuration, but are open to change.


## Control Tower and LDAP

Considering the need to pull information together from
a number of sources, the Control Tower relies on LDAP
for configuration information.  This is the only
standard database protocol, and it has a good level of
semantic detail, so it is the best option to connect
identity providers and service providers.  Moreover, it
is extensible without risk of clashes when various
parties each make their own extensions.

LDAP also offers standardised
[content synchronisation](https://datatracker.ietf.org/doc/html/rfc4533)
which is generally known as SyncRepl.  This protocol
basically sends a search request and says *when done,
please keep me posted on changes*.  It also has a
facility to avoid starting from scratch on every
connection setup with this.

Generic tools for LDAP are sparse, but the ones
that implement a particular use case are abundant and
they tell a clear story; namely that, once it works,
they work reliably and with good efficiency.  When
we speak of our IdentityHub container for example,
it means a central LDAP database for domain-bound
identities, together with commands to inject new
ones in a simple shell interaction, but alternatively
via a JSON message supplied over HTTP, and so on.
LDAP quietly takes care of it and updates listeners
that asked to stay up to date.


## Control Tower and SteamWorks Pulley

We designed a set of components by the name SteamWorks,
of which the most promising is Pulley.  This pulls
data in, combines data fields through conditions, and
drives outputs into ARPA2 Groups and Access Control via
the Rules DB driver; into workflow processes via the
Lifecycle driver; and into a large variety of container
configuration files via backends such as the Lua drive.
These Pulley Backend drivers are collected in the
PulleyBack software package.

Generally, the Control Tower can be said to run Pulley
on a number of `.ply` scripts and deliver the output
via various backends to any number of containers.
Where necessary, flexibility has been allowed through
relatively simple scripts.  It is up to Pulley to ensure
the tight and coordinated behaviour that brings these
simple actions together into the concerted behaviour of
a hosting site under the InternetWide Architecture.

