<!-- SPDX-FileCopyrightText: 2019 Adriaan de Groot <groot@kde.org>
     SPDX-License-Identifier: BSD-2-Clause
  -->

# Pulleyback

> Pulley backends (for Steamworks) for all the ARPA2 components in a fully-deployed system.

## Background

[Steamworks](https://gitlab.com/arpa2/steamworks) is a system
for distributing (configuration) data from LDAP to endpoints
through components *Crank*, *Shaft* and *Pulley*.
The pulleys are the end of the chain: they transform 
LDAP data into something usable locally.

A Pulley is configured with (one or more) *pulley scripts*
which do data matching and transformation and then
use *pulley backends* to do the real work. A backend
is a (shared library) plugin that is loaded by the
pulley process.

This repository holds a collection of pulley backends
(and example scripts) used by ARPA2 projects: IdentityHub, Reservoir
and others.

## Pulley Backends

 - *lifecycle*

## Building

There is a convenience `Makefile` which invokes cmake
with suitable arguments. For inside information,
look in the `CMakeLists.txt`.
