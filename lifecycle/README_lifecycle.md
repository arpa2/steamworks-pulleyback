# Pulley Backend for Life Cycle Management

<!-- SPDX-FileCopyrightText: 2018 Rick van Rein <rick@openfortress.nl>
     SPDX-License-Identifier: BSD-2-Clause
  -->

> *This is a plugin for SteamWorks Pulley, a so-called "pulleyback" module.
> Its task is to process attributes that indicate life cycle state, to
> evolve them as time and other life cycles evolve, and signal an external
> program to make steps forward.*

There are many places where process coordination can be helpful:

  * The management of keys is inherently complex, except on a small or manual
    scale.  As soon as you scale up, you want to automate.  At that time, the
    entanglements of procedures for DANE, ACME, DNS caches and internal service
    setups can be incredibly difficult.

  * The management of hosts is somewhat complex.  A host needs to be
    provisioned, installed, brought up, updated, snapshotted -- in that
    order.

  * Welcoming a new person in an organization is somewhat complex.  There's
    HR and IT processes to deal with, as well as a tour of the building and
    some physical infrastructure that needs to be done. Some of the steps
    are inter-dependent (you can't get a key-fob before you have an account).

*Life Cycle Management* was created to solve that line of problems.  It
expresses the processes for these kind of management issues in a way
that can support multiple concurrent processes in LDAP.
The ideas behind life cycle management come from
Communicating Sequential Processes and can be applied to many
sequential changes that need to be done to a single object in LDAP.




## Storage in LDAP

Attributes named `lifecycleState` can be attached to any `lifecycleObject`
in LDAP to capture sequential progression along named lines of action.  Using the
[SteamWorks](https://github.com/arpa2/steamworks)
subscriber module named
[Pulley](https://github.com/arpa2/steamworks/blob/master/docs/pulley.md)
for which the Life Cycle Management system is a mere
[PulleyBack module](https://github.com/arpa2/steamworks/blob/master/docs/pulleyback-api.md)
that processes added and removed `lifecycleState` attributes.

LDAP guarantees atomic updates of individual objects.  This is why it is
useful to store managed data together with the `lifecycleState` attributes
that constrain the related processing order.  That allows the
update of both data and `lifecycleState` at the same time.  LDAP is
perfectly suited for such combined data objects, by attaching an auxiliary
object class `lifecycleObject` that introduces the `lifeycleState` attribute;
it also welcomes as many values for this attribute as desired.

The configuration of Pulley is done in a so-called
[PulleyScript](http://steamworks.arpa2.net/intuition.html)
that delivers the desired selection of object changes to an output driver.
This means that it is possible to focus on lifecycle actions under constraints
based on other LDAP content.  Even with a fixed pulleyback handler it is
possible for an administrator to regulate the range to which it applies.

## Triggering Actions

The PulleyScript is configured with output drivers.  For
Life Cycle Management, these drivers will be sent two bits
of information, namely an object's `distinguishedName` and
the respective `lifecycleState` that has changed.

The output drivers can configure different life cycle names,
and provide a command for each by saying things like

```
dn,lcs -> lifecycle (x509="/bin/certpush.sh", dane="/bin/dane2dns.py")
```

The `x509` and `dane` names are life cycle names, and would be
matched by the first word in `lcs`, as that is the name of the
life cycle represented in that `lifecycleState` attribute.
The paths that follow indicate the shell command to start.
These commands read sets of two lines at once; the first is to
hold the `dn` value, the second the `lcs` value.

Other than this, the programs can do anything.  They can be
made in any language.  But in any case, they are suggested
to make changes to update the object indicated by the `dn`
to demonstrate progress, specifically the `lcs` value should
be updated (as it contains a `.` to split past and future,
and that should be moved forward).  If left without update,
the request will be repeated with exponential fallback.

As a tip, an easy mechanism for updating LDAP may be the
[Crank](https://github.com/arpa2/steamworks/blob/master/docs/crank.md)
because of its FastCGI interface that can be addressed
directly or through any supporting web server.


## Notes on the examples

### Key management

 various aspects of key management as
sequential processes with a bit of synchronisation from time to time.
These processes are aware of time, and can express things like DNS delays.

As long as the synchronisation points are well-defined and the application
takes good care of things like
[DNS cache update delays](https://github.com/arpa2/docker-demo/tree/master/demo-dns),
the result would be a robust system for generating, sharing and updating
public credentials.


### Example Pulley Script: X.509 Certificates

**TODO**


### Example Flow: X.509 with DANE and ACME

**TODO**

