#! /bin/sh
#
# Example lifecycle script. A lifecycle script reads from stdin
# and gets 2 lines for each change.
exec /bin/cat > /tmp/lifecycle.$$.log
