#!/bin/bash
#
# Read DN,LCS line pairs and produce certificates for it.
#
# This is a partial example; it responds with an LDAPmodify
# file on stdout (and possibly extra info on stderr).
#
# uid=john,accessDomain=example.org
# mkcert . keygen selfsign
#
# uid=john,accessDomain=example.org
# mkcert keygen . selfsign
#
# From: Rick van Rein <rick@openfortress.nl>


# Configuration variables
#
BASEDIR=/tmp/lc2cert


# ldapmodify "$DN" "$LCS_OLD" "$LCS_NEW"
#
ldapmodify() {
	DN="$1"
	LCS_OLD="$2"
	LCS_NEW="$3"
	if [ -z "$LCS_OLD$LCS_NEW" -o -z "$DN" ]
	then
		return
	fi
	echo "dn: $DN"
	echo "changetype: modify"
	if [ -n "$LCS_OLD" ]
	then
		echo "-"
		echo "delete: lifecycleState"
		echo "lifecycleState: $LCS_OLD"
	fi
	if [ -n "$LCS_NEW" ]
	then
		echo "-"
		echo "add: lifecycleState"
		echo "lifecycleState: $LCS_NEW"
	fi
	echo
}


# Setup the CA if it does not exist yet
#
if [ ! -r "$BASEDIR/mycapriv.pem" ]
then
	mkdir -p "$BASEDIR/demoCA"
	openssl req -batch -new -x509 -days 3660 \
		-nodes -newkey rsa:4064 \
		-keyout "$BASEDIR/mycapriv.pem" -out "$BASEDIR/mycacert.pem" \
		-subj "/OU=CertificateAutority/O=mkcert.sh"
	# -config "${CMAKE_BINARY_DIR}/ca/openssl.cnf"
fi


# Main loop
#
while true
do

	# Read an input pair of (DN,LCS) lines
	#
	read DN
	if [ -z "$DN" ]
	then
		exit 0
	fi
	read LCS
	if [ -z "$LCS" ]
	then
		exit 1
	fi

	# Turn the DN into a certificate path
	#
	DIR=""
	SSLDN=""
	for COMPO in $(echo -n "$DN" | sed -e 's+ +%20+g' -e 's+/+%2f+g' -e 's+,+ +g')
	do
		DIR="$COMPO/$DIR"
		SSLDN="/$COMPO$SSLDN"
	done
	DIR="${BASEDIR%/}/${DIR%/}/"
	mkdir -p "$DIR"

	# Switch to an action handler
	#
	LCS2=$(echo "$LCS" | sed 's/^\(.*\) [.] \([^ ]*\)/\1 \2 ./')
	case "$LCS " in

	# Generate a private key
	#
	"mkcert . keygen "*)
		openssl genrsa -out "$DIR/priv.pem" 2048
		ldapmodify "$DN" "$LCS" "$LCS2"
		;;

	# Create a self-signed certificate for a generated private key
	#
	"mkcert "*" . selfsign "*)
		openssl req -x509 -days 35 -key "$DIR/priv.pem" -out "$DIR/self.pem" -subj "$SSLDN" -utf8
		ldapmodify "$DN" "$LCS" "$LCS2"
		;;

	# Create a certificate request from a generated private key
	#
	"mkcert "*" . request "*)
		openssl req -new  -days 35 -key "$DIR/priv.pem" -out "$DIR/req.pem"  -subj "$SSLDN" -utf8
		ldapmodify "$DN" "$LCS" "$LCS2"
		;;

	# Create a local-CA-signed certificate for a certificate request
	#
	# TODO: Stuff with openssl.cnf remaining, and the CA setup
	#
	"mkcert "*" . mycasign "*)
		mkdir -p demoCA/newcerts
		openssl ca -days 35 -in "$DIR/req.pem" -cert "$BASEDIR/mycacert.pem" -keyfile "$BASEDIR/mycapriv.pem" -out "$DIR/cert.pem"
		;;

	# Unrecognised commands will trigger a complaint
	#
	*)
		echo 2>&1 Unrecognised command
		;;

	# End of action switching
	#
	esac

done

