# Pulley Backend for BIND9 Secondaries

> *This backend supports configuration of secondary DNS with
> BIND9 as the name server daemon.*

This backend is written in Lua, and its code is in
`lua/src/pulleyback_bind9slave.lua`.  Like all Lua backends,
it implements full two-phase transaction logic.

The script is used as an [example for a Lua backend](README.md).

Load from `.ply` with an output driver line that supplies a
domain name attribute,

```
domain -> bind9slave (...)
```

The `...` mark the configuration parameters, decribed below.


## Configuration Parameters

The following configuration parameters are currently available:

  * `dryrun` will not cause any actual changes, regardless of
    its value.  Take note that saying `dryrun="0"` or `dryrun="no"`
    and `dryrun="false"` all request a dry run!

    **Be careful** with this; transaction logic is in full swing,
    so this will cause confirmations for changes not made.  This
    may cause confusion in the remainder of the Pulley backend.
    It is often better to produce an alternative configuration file
    instead; one that is not being loaded by the BIND9 daemon.

  * `conffile` is a single configuration file that will list the
    zones to enslave to.  This is the filename that BIND9 is
    configured to (re)load.  The entries written will assume some
    context; see below.  The default value `/etc/bind/pulley.conf`
    will be used when no `conffile` is specified.

  * `prepfile` is a working version of the `conffile` being
    prepared.  This work is done during prepare-for-commit, and
    effectuated during transaction-commit or torn down during
    transaction-rollback.  The default value is the same as
    `conffile` with a `.new` extension added.

  * `cachedir` is the directory used to store zone files as
    downloaded from the DNS master.  If the directory does not
    exist or cannot be written by BIND9, then runtime errors
    may be expected.  The default value `/var/cache/bind`
    is used when no `cachedir` is specified.


## How to setup BIND9 and its Context

This Pulley Backend assumes that it can trigger the BIND9 daemon
with the following procedure:

    rndc reconfig || killall -HUP named

When both fails, the configuration would not be updated.  The
advised setup is throught `rndc`; it complains upon failure,
whereas `killall` is asynchronous notification and therefore
silent.  When BIND9 is not running, it would pickup on changes
in the configuration when restarted, so that is no concern to
this backend.

When a configuration change is triggered, the file specified in `conffile`
or otherwise the default `/etc/bind/pulley.conf` should be loaded,
possibly by inclusion into another configuration file.

The configuration file produced by the backend is a list of entries
of the form

```
zone "example.org" {
	type slave;
	file "/var/cache/bind/example.org.zone";
	masters { primary; };
};
```

where `example.org` can be any zone name and `/var/cache/bind` is
taken from the `cachedir` configuration variable described above.

Note the `primary` reference to the master DNS server.  This is
supposed to be setup in the configuration context, usually as part
of a configuration file that loads the file from this Pulley Backend.
A fragment of this file could be

```
masters "primary" {
        2001:db8:53::1;
};

key "symkey" {
        algorithm hmac-sha256-128;
        secret "DYmHcumaAlm3i6NLWNlg3mnu1udlqdCObxMFxJcCc24=";
};

server 2001:db8:53::1 {
        keys { symkey; };
};

include "/etc/bind/pulley.conf";
```

where `/etc/bind/pulley.conf` would be the `conffile` specified
in the `.ply` file or, for this value, be left as the default.
