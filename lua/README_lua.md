# Pulley Backend for Lua Updated Config

> *Lua is a good fit for embedded scripts.  And its modest library seems
> to suffice for generating configuration files and kick daemons, with
> simple handling of dynamic structures, but without the bloat of some
> of the other scripting languages.*

Lua scripting offers a modest environment, but it greatly simplifies the
handling of dynamic data.  Many uses of Pulley involve the creation of
textual configuration files and triggering daemons to reload them.  This
sort of text handling is not as easy in C as, say, addressing the API of
a database library.  Lua can fill in this gap and grant more flexibility
in what ends up in the configuration files.


## Almost the same API

The API avaliable over Lua is mostly the same as for C.  Functions use
the same names and most return 1 for success or 0 for failure.  The
wrapper makes only very small changes:

  * The customary `pulleyback_open (argv, argc, varc)` function is wrapped
    as `pulleyback_open (args, varc)` such that the `cfvar="value"`
    definitions in `argv` and `argc` map to a Lua table `args`, with
    entries `args.cfvar == "value"` in it.  This means that scripts can write
    things like

        self.dryrun   = not args.conffile
	self.conffile = args.conffile or os.tmpname ()
        self.tempfile = args.tempfile or self.conffile .. ".new"

  * The returned value from `pulleyback_open()` is treated like a `self`
    structure.  If it is `nil` then opening is considered to have failed.
    Later calls to the API functions are handed this value as a first
    argument.  This is similar to the `void *pbh` handling in the C API.

  * The DER structures passed into `pulleyback_add()` and `pulleyback_del()`
    are unpacked, and turned into function arguments.  Most scripts will set
    a fixed `varc` value and can define functions like

        pulleyback_add = function (self, dn, lcs)
            ; -- handle dn, lcs
        end

    Other applications may consider variable inputs, and would store `varc`
    in a variable like `self.varc` but they might also benefit from the fact
    that `nil` values mark the end because Pulley always supplies a value,
    even if just an empty string, like in

        pulleyback_add = function (self, dn, lcs, signal)
            ; -- handle dn, lcs
            if signal then
                ; -- in addition, handle the signal
            end
        end
    
    It is also possible to use Lua's capability for handling varargs, like

        -- TODO: Grammar not entirely correct yet.  Lua version dependent?
        pulleyback_del = function (self, dn, lcs, ...)
            ; -- handle dn, lcs
            for argv in ... do
                ; -- in addition, handle argv
            done
        end

  * Lua scripts must support two-phase transactions, which is simpler than
    might be expected, as explained below.


## Examples of Backends

The following backends are included in the `lua/src` directory,
with gradually progressing tests in `lua/test`,

  * [Null backend](src/pulleyback_1st.lua)
    does nothing but printing output.

  * [Bind9 as a DNS slave](src/pulleyback_bind9slave.lua)
    writes a configuration file with zones to be cached, which
    can be included into the main Bind9 configuration file.

The advised development trajectory is to setup a similar stepwise
test buildup, initially commenting out the later tests with
`if (CUT_THROAT_TESTING)` around each test, and then removing those
to enter a next phase of development and testing.  This cycle can
produce reliable code in a few hours.


## Transaction Logic

Pulley is designed to deliver changes reliably.  This includes the
transactional logic that ensures that a change has either been
completely confirmed, or not at all.  Multiple changes may pile up
to one large unit to commit or rollback.

Two-phase transactions are the only way to do this properly across
multiple backend modules.  Imagine a first succeeding and a second
failing, then what to do?  The answer is to first prepare a commit
until it can certainly be finished, do this on the second backend
too, and only when all agree to actually commit; if one fails, the
alternative applies, namely to rollack all the changes.  This is a
recipe for reliability.

It may sound difficult to accomplish, but it really is not.  For
example:

  * Lua supports transactional configuration changes in OpenWRT
  * A `os.rename()` on configuration files already is atomic
  * Links to configuration files can be redirected atomically
  * Daemons often need a signal or message to reload a prepared configuration

In other words, such things can easily be accomplished.  Take a look
at the `src/pulleyback_bind9slave.lua` script for how this can be
done:

  * The `bind9slave` script manages a list of zones to serve;
    it outputs a configuration to `args.conffile`
  * The `self.oldzones` represents zones that are currently serviced
  * The `self.newzones` is prepared as a new serviced-zone list
    with calls like
    `pulleyback_reset()`, `pulleyback_add()`, `pulleyback_del()`.
    Feedback like *zone already known* can be given on the basis
    of this `self.newzones` state
  * To prepare for commit, `pulleyback_prepare()` writes a file
    `args.prepfile` with the new configuration.  It should also
    test if it has write access to the eventual configuration
    file `args.conffile`.  If all this works, it returns 1 to
    signal success.
  * Upon transaction failure, `pulleyback_rollback()` is called
    to cleanup the `args.prepfile` if it was already written
    during `pulleyback_prepare()`.  It forgets changes to
    `zones.newzones` by copying its value from `zones.oldzones`.
  * Upon transaction succes, `pulleyback_commit()` is called to
    atomically move `args.prepfile` over `args.conffile` and then
    signal the BIND9 daemon to reload the configuration.  It then
    updates `self.oldzones` by copying its value from the new
    `self.newzones` situation.

This work is not really difficult, it merely represents attention
to stability by implementing the work in a few well-defined
functions.  It is strongly encouraged to do this faithfully and
not to "leave it be for now" because that endangers stability.

We are interested in adding scripts written by others, especially
to accommodate our "mkroot" environment, but we will always be
mindful about transaction support so that any such risks cannot
spread through our repository.


## Dynamic Script Loading

Dynamic script loading allows a very fast development cycle.  Plus, the
translation to bytecode in Lua only comes with a penelty during a first
load.  For this reason, a special wrapper was made to load scripts
dynamically when an output driver is instantiated by Pulley.

The backend doing this is `src/pulleyback_lua.lua` which expects a
config parameter `_script="name"` naming the script to load.  It will
add prefix and extension to form a name like `pulleyback_name.lua` to
load from a trusted directory.  By default, this is the `lua` sudirectory
underneath the trusted directory for Pulley Backend plugins.

This backend defines a Lua function `pulleyback_open()` which loads
the given script to replace the definitions in its global environment
with the Pulleyback API functions.  Even `pulleyback_open()` is
overridden, and then called with the same `args` and `varc` to yield
the intended output driver reference.

Future calls to `pulleyback_open()` will directly find the dynamically
loaded function, without reloading.  This means that the efficiency of
the resulting construction is good, at the expense of having to restart
Pulley to reload the script.  The development cycle mostly uses the
PulleyScript backend, which mostly runs in one-shot mode, often from
CTest runs, so this should not pose a serious problem.


## Static Script Loading

The management of self-contained backends is easier than the split
into a Lua backend and a Lua script from another directory.  To
simplify administration and minimise security risks, it is possible
to build a self-contained backend holding the Lua wrapper with the
Lua script.

To this end, a `pulleyback_new.lua` script is added to the `lua/src`
directory and its `new` name is added to the `LUA_COMPILED_MODULES`
list in `lua/src/CMakeLists.txt`.  It should then build and install
like any other Pulley Backend.  This is also a good point to write
a `lua/README_name.md` and add it to `doc/CMakeLists.txt` for
addition to the [Pulleyback Documentation Site](http://pulleyback.arpa2.net/).

The Lua code does not change to accommodate this.  But the loading
from PulleyScript changes.  If your `.ply` used to say something like

```
attr0, attr1 -> lua (_script="new", cfvar0="value0", cfvar1="value1")
```

then it can indicate direct loading with

```
attr0, attr1 -> new (cfvar0="value0", cfvar1="value1")
```

Under the hood, a simple program `lua2cstring.lua` maps the file
with Lua code to a C loader function `loadlua()`, which is called
from the wrapper.  The variation between the Lua modules is only
what is setup in these `loadlua()` functions.

On a side-note, the dynamic loader module `pulleyback_lua.so`
is built in precisely this manner.  The loading takes place in
the Lua realm, and exploits the dynamicity of the Lua environment
to replace the loader with loaded script.  This is expressed in
the `lua/src/pulleyback_lua.lua` script, whose only difference
is that it comes with a `.in` extension, so CMake can set it up
with a configurable directory to trust as a Lua script location.


## References

  * [Lua reference manual](https://www.lua.org/manual/5.4/manual.html)

  * [Examples of embedding Lua in C](https://lucasklassmann.com/blog/2019-02-02-how-to-embeddeding-lua-in-c/)

  * [The pitfalls of loading Lua code](https://www.corsix.org/content/common-lua-pitfall-loading-code)

