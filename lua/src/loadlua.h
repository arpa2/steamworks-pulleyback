/* Lua fetcher.  Returns true on success, false on failure.  Usually generated.
 */

#include <stdbool.h>

#include <lua.h>
#include <lauxlib.h>

bool loadlua (lua_State *luastate);

