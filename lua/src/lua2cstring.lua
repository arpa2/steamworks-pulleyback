-- Lua file to a loadlua() with an embedded C string
--
-- This program reads a source code file in Lua and outputs it in C.
-- The form is a C string, and intended to remain readable.
--
-- The purpose of this is to easily move from a flow with a separate
-- Lua script for the dynamically loading Pulley Backend to one that
-- embeds the same script in a Pulley Backend of its own, to be
-- installed into the standard directory for Pulley Backends.
--
-- From: Rick van Rein <rick@openfortress.nl>



-- Escape special characters in hex
--
ashex = function (c)
	return string.format ('\\x%02x', c:byte (1,-1))
end


-- Map a Lua source file to a C header file
--
lua2c = function (luafile, cfile)
	out = io.open (cfile, "w")
	out:write ("/* This is a generated Lua string\n *\n * EDITING DONE HERE WILL BE OVERWRITTEN\n */\n\n\n#include \"loadlua.h\"\n\n\nstatic const char *lua_source =\n")
	for line in io.lines (luafile) do
		line = line:gsub ("\\", "\\\\")
		line = line:gsub ("\"", "\\\"")
		line = line:gsub ("[\x00-\x08\x0a-\x1f\x7f-\xff]", ashex)
		out:write ("\t\"" .. line .. "\\n\"\n")
	end
	out:write (";\n\n\nbool loadlua (lua_State *luastate) {\n\treturn luaL_loadstring (luastate, lua_source);\n}\n\n\n/*\n * End of generated Lua string\n */\n");
	out:close ()
end


-- Main program: Map one .lua file to one .c file
--
if #arg ~= 2 or arg[1]:sub (#arg[1] - 3) ~= ".lua" or arg[2]:sub (#arg[2] - 1) ~= ".c" then
	print ("Usage:", arg [0], "module_in.lua module_out.c")
	os.exit (1)
end
lua2c (arg[1], arg[2])
print ("Mapped", arg[1], "to", arg[2])

