/** @page PulleyBack API translator to Lua.
 *
 * Lua is a small but potent backend library.  It could be most useful
 * to allow administrators to deliver pulled data from LDAP in their
 * local systems.
 *
 * Compared to C programs, Lua is much easier to use and specifically
 * easier to modify.  It sits somewhere between programming and scripting.
 *
 * Compared to shell scripts, Lua is much more a programming language,
 * with support for binary data and easier to use structures.  It is
 * a better mechanism for constructing reliable backends.
 *
 * Compared to Python scripts, Lua is much smaller, thus making it a
 * better choice for container environments.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */

/*
 *   SPDX-FileCopyrightText: 2021 Rick van Rein <rick@openfortress.nl>
 *   SPDX-FileCopyrightText: 2021 InternetWide.org and the ARPA2.net project
 *   SPDX-License-Identifier: BSD-2-Clause
 */


#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include <errno.h>

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include <steamworks/pulleyback.h>

#include "loadlua.h"



static const char logger[] = "steamworks.pulleyback.lua";


/*
 * The documentation below can be passed through Doxygen to produce a
 * Lua variant of the PulleyBack API.
 */


/** @section pulleyback-init Initialisation and Cleanup
 *
 * Once the Lua script is loaded, output drivers may be openend as
 * independently operated instances.  The opening and closing calls for
 * instances are:
 *
 *     pulleyback_open  = function (varc, args);  -- constructs self
 *     pulleyback_close = function (self);        -- returns void
 *
 * The @a varc value indicates how many attribute/dn values will
 * be passed into later calls to pulleyback_add() and pulleyback_del().
 *
 * The table of @a args has a key and value for every `key="value"`
 * configuration parameter for this output driver.  Every instance
 * may well have its own set of values.  Compared to the C interface,
 * table structure implies that the order of arguments is lost and
 * that the same `key` may not occur more than once.
 *
 * Successfully opened output drivers return a PulleyBack Handle,
 * referred to as `self` below.  Its type is completely determined
 * by Lua; only the value `nil` is interpreted as a failure to open.
 */

/** @section pulleyback-fork Adding and Removing Forks
 *
 * The primary function of a backend is to have forks added to and removed from
 * an instance.  This is done with the respective functions
 *
 *     pulleyback_add = function (self, ...);  -- returns 1/0
 *     pulleyback_del = function (self, ...);  -- returns 1/0
 *
 * The `pbh` is a PulleyBack Handle from pulleyback_open() and the
 * `...` passes the Pulley variables sent to the output driver as
 * a tuple of values that exist in the same fork.  The number of actual
 * parameters is presented in the `varc` argument to pulleyback_open()
 * and the contents are never `nil`, so they are easily distinguished
 * from any fillers in case of dynamicity.
 *
 * These functions return 1 on success and 0 on failure; any failure status
 * is retained within an instance, and reported for future additions and
 * removals of forks, as well as for attempts to commit the transaction.
 * Because none of the changes is made instantly, they are stored as part
 * of a current transaction, which is always implicitly open.
 *
 * Note that Pulley keeps track of the count of guards for a given set of
 * `forkdata` values.  It will avoid invoking `pulleyback_add()` more than
 * once on the same `atval` tuple without first having called `pulleyback_del()`
 * on it.  It will also avoid calling `pulleyback_del()` on the same `atval` tuple
 * unless they have been added with `pulleyback_add()` and since not removed by
 * `pulleyback_del()`.  These statements do apply over a sequence of sessions,
 * each of which is marked by loading and unloading the backend plugin module.
 */

/** @section Transaction Processing Support
 *
 * Transactions are used in the Pulley Backend to release all information
 * at the same time.  This makes it possible, for instance, to remove something
 * and add it again, without it disappearing from the external view.
 *
 * Ideally, all backend plugins would have two-phase commit facilities, but
 * not all underlying structures will be able to support this.  It is
 * possible to achieve the same level of certainty with any number of
 * two-phase and a single one-phase backend, so it is useful to know
 * a backend's support for two-phase commit.  We do this by making this
 * backend available in two variations, `lua` and `lua_solo`.
 *
 * The following API functions support transactions on an open instance:
 *
 *     pulleyback_prepare  = function (self);  -- returns 1/0
 *     pulleyback_commit   = function (self);  -- returns 1/0
 *     pulleyback_rollback = function (self);  -- returns void
 *
 * The functions implement prepare-to-commit, commit and rollback, respectively.
 * When only single-phase commit is supported, then `pulleyback_prepare()` will
 * not resolve, which is permitted as it is marked optional.  The result from
 * `pulleyback_prepare()` predicts the success of an upcoming
 * `pulleyback_commit()`, but still makes it possible to run
 * `pulleyback_rollback()` instead.  When `pulleyback_prepare()` succeeds
 * then the following `pulleyback_commit()` must also succeed; in fact, the
 * calling application is under no obligation to check the result in that case.
 *
 * Invocations of `pulleyback_prepare()` are idempotent, meaning that they
 * may be repeated and will then return the same result.  Afterwards, one
 * call to either `pulleyback_commit()` or `pulleyback_rollback()` can be
 * made.  Using `pulleyback_commit()` or `pulleyback_rollback()` when no
 * transaction was implicitly created responds equivalently to going through
 * a transaction without any changes made in it.  When `pulleyback_close()`
 * is called after `pulleyback_prepare()` then, regardless of success or
 * failure having been returned from it, the usualy implicit call to
 * `pulleyback_rollback()` is conducted.
 *
 * It is a wrong use of this API to operate on data between a
 * `pulleyback_prepare()` and either `pulleyback_commit()` or
 * `pulleyback_rollback()`; it is wrong to call `pulleyback_commit()`
 * after a failed `pulleyback_prepare()` but it is not wrong to
 * call `pulleyback_rollback()` after a successful `pulleyback_prepare()`.
 *
 * When either `pulleyback_prepare()` or `pulleyback_commit()` fails, it
 * sets `errno` to give an idea why.  It may specifically be useful to check
 * for the `EAGAIN` value.  This is the designated return value in cases
 * where a transaction runs into a deadlock.  In a single-threaded Pulley
 * this should not happen, but it might in a multithreaded future version,
 * and backends should already be prepared to inform such future versions
 * with this special return value.
 *
 * @subsection Normal Transactional Sequence
 *
 * The normal Pulley sequence is to perform `pulleyback_prepare()` on all
 * backends, and when all succeed to run `pulleyback_commit()` on them,
 * and otherwise run `pulleyback_rollback()` on all of them.
 *
 * It is permitted to invoke `pulleyback_rollback()` or `pulleyback_commit()`
 * on an instance
 * without prior call to `pulleback_prepare()`, in which case their outcome
 * is effective immediately, and the update is done as atomically as possible.
 *
 * It is not permitted to invoke anything but `pulleyback_close()`,
 * `pulleyback_commit()` or `pulleyback_rollback()` on an instance
 * after `pulleyback_prepare()` has been invoked.
 *
 * Note that `pulleyback_close()` does an implicit `pulleyback_rollback()`
 * on any outstanding changes.  Please do not rely on this though, it is
 * only there as a stop-gap measure for unexpected program termination.
 *
 * @subsection Sharing Transactions between Backend Instances
 *
 * Backends may support an addition function to support transactions that
 * run over multiple instances of the same backend:
 *
 *     pulleyback_collaborate = function (self, other);  -- returns 1/0
 *
 * This expresses an intent to use one transaction for the two backends.
 * The return value is 1 for success and 0 for failure; the value `errno`
 * will not be set upon failure, since it is something determined inside
 * the implementation, possibly as a result of independent transactional
 * scopes -- for example, separate database contexts or environments.
 *
 * Upon success, an application only needs to end the transaction on
 * `self` or `other`; doing it on both is superfluous but, in light of the
 * idempotence of the transaction-ending operations, it is also harmless.
 *
 * One way of using this is to maintain a list (or bitfield) with the backends
 * that are involved in a transaction as a unique participant.  As soon as
 * a new backend is added, use `pulleyback_collaborate()` to attempt to ask
 * the new backend to collaborate with any of the existing transactions, until
 * one succeeds.  Only when all collaboration attempts fail will the backend
 * be added to the list (or bitfield) with the backends that are involved in
 * a transaction as a unique participant.  When ending the transaction,
 * invoke the operations only on the backends that are in this list (or bitfield).
 * To implement this scheme, there probably is a need to also keep a list
 * (or bitfield) of collaborators, just to make sure that it isn't asked to
 * pair over and over again.
 *
 * The implementation of this facility, as well as its grounds on which
 * acceptance or rejection is formed, is entirely up to the backend.  This
 * is why it is not optional -- it can easily return 0 in all cases, if it
 * wants to.
 */


/* The storage for this Pulley Backend is the proxy data for Lua.
 */
struct luaproxy {
	lua_State *luastate;
	int varc;
};


#if 0
/* The default allocator function from the Lua manual
 */
static void *l_alloc (void *ud, void *ptr, size_t osize, size_t nsize) {
	(void)ud;  (void)osize;  /* not used */
	if (nsize == 0) {
		free(ptr);
		return NULL;
	} else {
		return realloc(ptr, nsize);
	}
}
#endif


/* Parse the pointer and length from a DER header.
 * Return success as true, failure as false.
 */
static bool parse_der (uint8_t *der, char **ptr, size_t *len) {
	der++;
	if ((*der & 0x80) != 0x00) {
		int lenlen = (*der & 0x7f);
		if ((lenlen < 1) || (lenlen > 2)) {
			return false;
		}
		*len = der [0];
		if (lenlen == 2) {
			*len = ((*len) << 8) | der [1];
			*ptr = (char *) (der + 2);
		} else {
			*ptr = (char *) (der + 1);
		}
	} else {
		*len = *der;
		*ptr = (char *) (der + 1);
	}
	return true;
}



/* Pass to Lua as
 *
 *     pulleyback_open  = function (varc, args);  -- constructor returning self
 *
 * TODO: DETERMINE WHAT SCRIPT MUST BE LOADED
 */
void* pulleyback_open (int argc, char** argv, int varc) {
	//
	// Allocate the luaproxy structure
	struct luaproxy *lp = calloc (sizeof (struct luaproxy), 1);
	if (lp == NULL) {
		write_logger (logger, "No memory to allocate Lua Proxy");
		errno = ENOMEM;
		return NULL;
	}
	//
	// Remember the varc value
	lp->varc = varc;
	//
	// Create a new Lua state object
printf ("LUA open AT %d\n", __LINE__);
	lp->luastate = luaL_newstate ();
printf ("LUA open AT %d\n", __LINE__);
	//SIMPLER// lp->luastate = lua_newstate (l_alloc, NULL);
	printf ("Size of stack is %d at %d\n", lua_gettop (lp->luastate), __LINE__);
	if (lp->luastate == NULL) {
		write_logger (logger, "Lua failed to allocate a state object");
		errno = ENOMEM;
		free (lp);
		return NULL;
	}
	//
	// Start checking correct handling
	bool ok = true;
printf ("ok = %d at %d\n", ok, __LINE__);
	//
	// Load the standard libraries
printf ("LUA open AT %d\n", __LINE__);
	luaL_openlibs (lp->luastate);
	printf ("Size of stack is %d at %d\n", lua_gettop (lp->luastate), __LINE__);
printf ("LUA open AT %d\n", __LINE__);
	//
	// Load the source or precompiled Lua program file
printf ("LUA open AT %d\n", __LINE__);
	// ok = ok && (luaL_dofile (lp->luastate, "pulleyback_1st.lua") == LUA_OK);
printf ("ok = %d at %d\n", ok, __LINE__);
printf ("LUA open AT %d\n", __LINE__);
	// ok = ok && (luaL_loadfile (lp->luastate, "pulleyback_bind9slave.lua") == LUA_OK);
printf ("ok = %d at %d\n", ok, __LINE__);
printf ("LUA open AT %d\n", __LINE__);
	int loadok = ok ? loadlua (lp->luastate) : LUA_OK;
printf ("loadlua() --> %d\n", loadok);
	if (loadok != LUA_OK) {
		size_t lstrlen;
		const char *lstrptr = luaL_tolstring (lp->luastate, 1, &lstrlen);
		fprintf (stderr, "Lua error: %.*s\n", (int) lstrlen, lstrptr);
		ok = false;
	}
printf ("ok = %d at %d\n", ok, __LINE__);
	printf ("Size of stack is %d at %d\n", lua_gettop (lp->luastate), __LINE__);
printf ("ok = %d at %d\n", ok, __LINE__);
printf ("LUA open AT %d\n", __LINE__);
	ok = ok && (lua_pcall (lp->luastate, 0, LUA_MULTRET, 0) == LUA_OK);
	printf ("Size of stack is %d at %d\n", lua_gettop (lp->luastate), __LINE__);
printf ("ok = %d at %d\n", ok, __LINE__);
printf ("LUA open AT %d\n", __LINE__);
	//
	// Push the Lua function
	//VERSION_SPECIFIC// ok = ok && (lua_getglobal (lp->luastate, "pulleyback_open") == LUA_TFUNCTION);
printf ("LUA open AT %d\n", __LINE__);
	lua_getglobal (lp->luastate, "pulleyback_open");
printf ("LUA open AT %d\n", __LINE__);
	printf ("Requiring that pulleyback_open() is a Lua function\n");
	fflush (stdout);
	assert (lua_isfunction (lp->luastate, -1));
	printf ("Size of stack is %d at %d\n", lua_gettop (lp->luastate), __LINE__);
printf ("ok = %d at %d\n", ok, __LINE__);
printf ("LUA open AT %d\n", __LINE__);
	//
	// Push the varc value
printf ("LUA open AT %d\n", __LINE__);
	lua_pushinteger (lp->luastate, varc);
printf ("LUA open AT %d\n", __LINE__);
	//
	// Build the table for argc key/value pairs
printf ("LUA open AT %d\n", __LINE__);
	lua_createtable (lp->luastate, 0, argc);
printf ("LUA open AT %d\n", __LINE__);
	for (int argi = 0; argi < argc ; argi++) {
		char *keystr = argv [argi];
		int keyvallen = strlen (keystr);
		char *split = strchr (keystr, '=');
		if ((split == NULL) || (split [1] != '"') || (split [2] == '\0') || (keystr [keyvallen-1] != '"')) {
			write_logger (logger, "Syntax error in configuration parameters to the output driver");
			errno = EINVAL;
			lua_close (lp->luastate);
			free (lp);
			return NULL;
		}
		int keylen = (split - keystr);
		char *valstr = split + 2;
		int vallen = (keyvallen - keylen - 3);
printf ("LUA open AT %d\n", __LINE__);
		lua_pushlstring (lp->luastate, keystr, keylen);
printf ("LUA open AT %d\n", __LINE__);
		lua_pushlstring (lp->luastate, valstr, vallen);
printf ("LUA open AT %d\n", __LINE__);
		lua_settable    (lp->luastate, 3);
printf ("LUA open AT %d\n", __LINE__);
	}
	//
	// Invoke the Lua function with (varc, args) to yield self
printf ("LUA open AT %d\n", __LINE__);
	printf ("Size of stack is %d at %d\n", lua_gettop (lp->luastate), __LINE__);
	lua_pcall (lp->luastate, 2, 1, 0);
	printf ("Size of stack is %d at %d\n", lua_gettop (lp->luastate), __LINE__);
printf ("LUA open AT %d\n", __LINE__);
	//
	// Return the result if it is not nil
printf ("ok = %d at %d\n", ok, __LINE__);
printf ("LUA open AT %d\n", __LINE__);
	ok = ok && !lua_isnoneornil (lp->luastate, -1);
printf ("ok = %d at %d\n", ok, __LINE__);
printf ("LUA open AT %d\n", __LINE__);
	if (ok) {
		/* We simply leave the self on top of the stack */
		return lp;
	}
	//
	// Cleanup and return NULL if things did not happen as intended
	write_logger (logger, "Lua failed to prepare the script as a Pulley Backend");
	lua_close (lp->luastate);
	free (lp);
	return NULL;
}

/* Pass to Lua as
 *     pulleyback_close = function (self);  -- returns void
 *
 * The stack is the PulleyBack Handle for Lua on calling,
 * and this function cleans that up.
 */
void pulleyback_close (void* pbh) {
	//
	// Initialise
	struct luaproxy *lp = pbh;
	//
	// Clean up in Lua
	lua_close (lp->luastate);
	//
	// Clean up our own structure
	free (pbh);
}

/* Most of the work for pulleyback_add() and pulleyback_del() is the same
*/
static int pulleyback_change (struct luaproxy *lp, der_t *forkdata) {
	//
	// Copy the "self" handle
	lua_pushvalue (lp->luastate, -2);
	printf ("Requiring that self is not nil\n");
	fflush (stdout);
	assert (!lua_isnoneornil (lp->luastate, -1));
	//
	// Iterate over the forkdata to push actual parameters
	for (int vari = 0; vari < lp->varc; vari++) {
		//
		// Extract the DER contents
		char *derval;
		size_t derlen;
		parse_der (forkdata [vari], &derval, &derlen);
		//
		// Add the DER-contained bytes to the stack
		printf ("Pushing %.*s\n", (int) derlen, derval);
		lua_pushlstring  (lp->luastate, derval, derlen);
	}
	//
	// Invoke the Lua function with (pbh, atvals) to yield 1/0
	printf ("Calling with %d args and %d returns\n", 1 + lp->varc, 1);
	lua_pcall (lp->luastate, 1 + lp->varc, 1, 0);
	//
	// Extract the Lua function's return state
	int retval_ok = 0;
	lua_Integer retval_lua = lua_tointegerx (lp->luastate, -1, &retval_ok);
	printf ("Returning %d if ok=%d\n", (int) retval_lua, retval_ok);
	int retval = retval_ok ? (retval_lua != 0) : 0;
	//
	// Return success or failure;
	printf ("Size of stack is %d at %d\n", lua_gettop (lp->luastate), __LINE__);
	lua_pop (lp->luastate, lua_gettop (lp->luastate) - 1);
	printf ("Size of stack is %d at %d\n", lua_gettop (lp->luastate), __LINE__);
	return retval;
}

/* Pass to Lua as
 *     pulleyback_add = function (self, ...);  -- returns 1/0
 *
 * The stack is the PulleyBack Handle for Lua on call and return.
 */
int pulleyback_add (void *pbh, der_t *forkdata) {
	//
	// Initialise
	struct luaproxy *lp = pbh;
	//
	// Push the Lua function
	printf ("Looking for pulleyback_add() in Lua\n");
	fflush (stdout);
	lua_getglobal (lp->luastate, "pulleyback_add");
	//
	// Check that we actually loaded a function
	printf ("Requiring that pulleyback_add() is a Lua function\n");
	fflush (stdout);
	assert (lua_isfunction (lp->luastate, -1));
	//
	// Continue into the shared part for add/del
	return pulleyback_change (lp, forkdata);
}

/* Pass to Lua as
 *     pulleyback_del = function (self, ...);  -- returns 1/0
 *
 * The stack is the PulleyBack Handle for Lua on call and return.
 */
int pulleyback_del (void *pbh, der_t *forkdata) {
	//
	// Initialise
	struct luaproxy *lp = pbh;
	//
	// Push the Lua function
	lua_getglobal (lp->luastate, "pulleyback_del");
	//
	// Check that we actually loaded a function
	assert (lua_isfunction (lp->luastate, -1));
	//
	// Continue into the shared part for del/add
	return pulleyback_change (lp, forkdata);
}

/* Simple calls with just the PulleyBack Handle as parameter.
 * These functions only differ by name, and whether they return
 * an integer value (or void, in which case 1 can be assumed).
 */
static int pulleyback_selfcall (struct luaproxy *lp, char *func_name, bool returns_int) {
	//
	// Push the Lua function
	lua_getglobal (lp->luastate, func_name);
	//
	// Check that we actually loaded a function
	printf ("Requiring that %s() is a Lua function\n", func_name);
	fflush (stdout);
	assert (lua_isfunction (lp->luastate, -1));
	//
	// Copy the "self" handle
	lua_pushvalue (lp->luastate, -2);
	printf ("Requiring that self is not nil\n");
	fflush (stdout);
	assert (!lua_isnoneornil (lp->luastate, -1));
	//
	// Invoke the Lua function with self to yield 1/0 _or_ void
	lua_pcall (lp->luastate, 1, returns_int ? 1 : 0, 0);
	//
	// Extract the Lua function's return state
	int retval = 1;
	printf ("Return an int from Lua if returns_int=%d\n", returns_int);
	if (returns_int) {
		int retval_ok = 0;
		lua_Integer retval_lua = lua_tointegerx (lp->luastate, -1, &retval_ok);
		printf ("Returning %d if ok=%d\n", (int) retval_lua, retval_ok);
		retval = retval_ok ? (retval_lua != 0) : 0;
	}
	//
	// Return success or failure;
	printf ("Size of stack is %d at %d\n", lua_gettop (lp->luastate), __LINE__);
	lua_pop (lp->luastate, lua_gettop (lp->luastate) - 1);
	printf ("Size of stack is %d at %d\n", lua_gettop (lp->luastate), __LINE__);
	return retval;
}

/* Pass to Lua as
 *     pulleyback_reset = function (self);  -- returns 1/0
 *
 * The stack is the PulleyBack Handle for Lua on call and return.
 */
int pulleyback_reset (void *pbh) {
	//
	// Invoke as a self-call
	return pulleyback_selfcall ((struct luaproxy *) pbh, "pulleyback_reset", true);
}

/* Pass to Lua as
 *     pulleyback_prepare = function (self);  -- returns 1/0
 *
 * The stack is the PulleyBack Handle for Lua on call and return.
 *
 * Note: This function is not optional to this Lua backend.
 *       All work is expected to be on file systems, which offer
 *       straightforward atomic semantics, namely move in the
 *       file system.  On top of that, daemons will often ignore
 *       changes to config files until a signal is sent.
 */
int pulleyback_prepare (void *pbh) {
	//
	// Invoke as a self-call
	return pulleyback_selfcall ((struct luaproxy *) pbh, "pulleyback_prepare", true);
}

/* Pass to Lua as
 *     pulleyback_commit = function (self);  -- returns 1/0
 *
 * The stack is the PulleyBack Handle for Lua on call and return.
 */
int pulleyback_commit (void *pbh) {
	//
	// Invoke as a self-call
	return pulleyback_selfcall ((struct luaproxy *) pbh, "pulleyback_commit", true);
}

/* Pass to Lua as
 *     pulleyback_rollback = function (self);  -- returns 1/0
 *
 * The stack is the PulleyBack Handle for Lua on call and return.
 */
void pulleyback_rollback (void *pbh) {
	//
	// Invoke as a self-call
	(void) pulleyback_selfcall ((struct luaproxy *) pbh, "pulleyback_rollback", false);
}

/* Maybe one day, pass to Lua as a environment-cross-referencing call
 *	pulleyback_collaborate = function (self, other);  -- returns 1/0
 *
 * TODO: No idea if Lua environments can mingle so easily.  Reject for now.
 */
int pulleyback_collaborate (void *pbh1, void *pbh2) {
	pbh1 = pbh1;
	pbh2 = pbh2;
	return 0;
}

