-- My first Lua plugin for Pulley


-- Construct a self object for future reference
pulleyback_open = function (varc, args)
	self = { }
	self.varc = varc
	self.args = args
	print ("pulleyback_open (", varc, ",", args, ") --> self")
	for k,v in pairs (args) do
		print (" args [", k, "] =", v)
	end
	print ("-->")
	for k,v in pairs (self) do
		print (" self [", k, "] =", v)
	end
	return self
end

-- Give up resources of the self object
pulleyback_close = function (self)
	print ("pulleyback_close (self)")
end

-- Add a fork to the backend described by the self object
pulleyback_add = function (self, ...)
	print ("pulleyback_add (self,", ..., ")")
	for k,v in pairs (self) do
		print (" self [", k, "] =", v)
	end
	print (" --> expected", self.varc, "parameters")
	return 1
end

-- Delete a fork from the backend described by the self object
pulleyback_del = function (self, ...)
	print ("pulleyback_del (self,", ..., ")")
	print (" --> expected", self.varc, "parameters")
	return 1
end

-- Reset the complete backend state under the self object
pulleyback_reset = function (self)
	print ("pulleyback_reset (self)")
	return 1
end

-- Prepare for commit under the self object
pulleyback_prepare = function (self)
	print ("pulleyback_prepare (self)")
	return 1
end

-- Commit the changes under the self object
pulleyback_commit = function (self)
	print ("pulleyback_commit (self)")
	return 1
end

-- Rollback the changes under the self object
pulleyback_rollback = function (self)
	print ("pulleyback_rollback (self)")
end

-- Collaboration is currently not supported
pulleyback_collaborate = function (self, other)
	print ("pulleyback_collaborate (self, other)")
	return 0
end

