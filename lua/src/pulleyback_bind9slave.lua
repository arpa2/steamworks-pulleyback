-- Generate BIND9 slave code
--
-- This is a Pulley Backend that configures DNS slave zones into a
-- BIND9 configuration file.  Everything is stored in one file.
-- Transaction preparation consists of writing a new config file in
-- a temporary name, transaction commit than uses an atomic rename
-- of the file to activate it.  Note that SIGHUP must then be sent
-- to BIND9 to make it notice this or, better, "rndc reconfig" should
-- be run.
--
-- From: Rick van Rein <rick@openfortress.nl>


-- Show the data in self and, possibly, a message/data sequence
--
log = function (self, msg, ...)
	if msg ~= nil then
		print (msg, ...)
	end
	print ("DEBUGLOG -->")
	for k,v in pairs (self) do
		print (" - self [", k, "] =", v)
		--WONTWORK print (string.format ("\tself [%q] = %q", k, v))
	end
end

-- Make a "deep" copy of a list of zones
--
copyzones = function (zonelist)
	newzones = { }
	for zk,zv in pairs (zonelist) do
		newzones [zk] = zv
	end
	return newzones
end

-- Construct a self object for future reference
--
pulleyback_open = function (varc, args)
	--
	-- Logging and checking
	log ({}, "bind9slave.pulleyback_open (", varc, ",", args, ") --> self")
	for k,v in pairs (args) do
		print (" - args [", k, "] =", v)
	end
	--
	-- Require varc==1, complain otherwise; ok/dryrun for varc<0 probes
	if varc ~= 1 then
		print ("Pulley Backend bind9slave.lua only wants an domain name")
		if varc >= 0 then
			return nil
		end
		args ["dryrun"] = "1"
	end
	--
	-- Create the self object using config args
	self = { }
	self.cachedir = args ["cachedir"] or "/var/cache/bind"
	self.conffile = args ["conffile"] or "/etc/bind/pulley.conf"
	self.prepfile = args ["prepfile"] or (self.conffile .. ".new")
	self.dryrun = (args ["dryrun"] ~= nil)
	--
	-- Initialise internal variables
	self.prepared = false
	self.txfail = false
	self.newzones = { }
	self.oldzones = { }
	--
	-- Remove any dangling prepfile
	if not self.dryrun then
		os.remove (self.prepfile)
	end
	--
	-- Logging of the new output driver instance
	log (self, "Created new output driver instance")
	--
	-- Return the new output driver instance
	return self
end

-- Give up resources of the self object
--
pulleyback_close = function (self)
	--
	-- Logging and checking
	log (self, "bind9slave.pulleyback_close (self)")
	--
	-- Cleanup any dangling prepfile
	if not self.dryrun then
		os.remove (self.prepfile)
	end
	--
	-- Return quietly
end

-- Add a fork to the backend described by the self object
--
pulleyback_add = function (self, domain)
	--
	-- Logging and checking
	log (self, "bind9slave.pulleyback_add (self,", domain, ")")
	if self.txfail then
		print ("Ignored: Transaction already marked as failed")
		return 0
	end
	if domain == nil then
		print ("Fatal: No domain given")
		self.txfail = true
		return 0
	end
	if self.prepared then
		print ("Ignored: Transaction prepared for commit")
		return 0
	end
	if self.newzones [domain] ~= nil then
		print ("Fatal: Domain", domain, "is already known!!")
		self.txfail = true
		return 0
	end
	--
	-- Add the domain to the list of new zones
	self.newzones [domain] = 1
	--
	-- Return success
	log (self, "--> return 1");
	return 1
end

-- Delete a fork from the backend described by the self object
--
pulleyback_del = function (self, ...)
	--
	-- Logging and checking
	log (self, "bind9slave.pulleyback_del (self,", ..., ")")
	if self.txfail then
		print ("Ignored: Transaction already marked as failed")
		return 0
	end
	if self.prepared then
		print ("Ignored: Transaction prepared for commit")
		return 0
	end
	if domain == nil then
		self.txfail = true
		return 0
	end
	if self.newzones [domain] == nil then
		print ("Fatal: Domain", domain, "is not known!!")
		self.txfail = true
		return 0
	end
	--
	-- Remove the domain from the list of new zones
	self.newzones [domain] = nil
	--
	-- Return success
	log (self, "--> return 1");
	return 1
end

-- Reset the complete backend state under the self object
--
pulleyback_reset = function (self)
	--
	-- Logging and checking
	log (self, "bind9slave.pulleyback_reset (self)")
	if self.txfail then
		print ("Ignored: Transaction already marked as failed")
		return 0
	end
	if self.prepared then
		print ("Ignored: Transaction prepared for commit")
		return 0
	end
	--
	-- Reset the list of new zones
	self.newzones = { }
	--
	-- Return success
	log (self, "--> return 1");
	return 1
end

-- Prepare for commit under the self object
--
pulleyback_prepare = function (self)
	--
	-- Logging and checking
	log (self, "bind9slave.pulleyback_prepare (self)")
	if self.txfail then
		print ("Ignored: Transaction already marked as failed")
		return 0
	end
	--
	-- Write the prepfile
	if self.dryrun then
		prepf = io.open ("/dev/null", "w")
	else
		prepf = io.open (self.prepfile, "w+")
	end
	for z,_ in pairs (self.newzones) do
		print (string.format (
			"zone \"%s\" {\n\ttype slave;\n\tfile \"%s/%s.zone\";\n\tmasters { primary; };\n};\n\n\n",
			z, self.cachedir, z))
		prepf:write (string.format (
			"zone \"%s\" {\n\ttype slave;\n\tfile \"%s/%s.zone\";\n\tmasters { primary; };\n};\n\n\n",
			z, self.cachedir, z))
	end
	prepf:close ()
	--
	-- Return success
	self.prepared = true
	log (self, "--> return 1");
	return 1
end

-- Commit the changes under the self object
--
pulleyback_commit = function (self)
	--
	-- Be sure to have the prepfile in place
	if not self.prepared and not self.txfail then
		if pulleyback_prepare (self) == 0 then
			return 0
		end
	end
	--
	-- Logging and checking
	log (self, "bind9slave.pulleyback_commit (self)")
	if self.txfail then
		print ("Ignored: Transaction already marked as failed")
		return 0
	end
	--
	-- Atomically move the conffile onto the prepfile
	if not self.dryrun then
		os.rename (self.prepfile, self.conffile)
	end
	--
	-- Tell BIND9 about the new zone file
	if not os.execute ("rndc reconfig") and not os.execute ("killall -HUP named") then
		print ("Failed to trigger BIND9 to reconfigure zones")
	end
	--
	-- Update the internal administration of self
	self.prepared = false
	self.oldzones = copyzones (self.newzones)
	if not self.dryrun then
		os.remove (self.prepfile)
	end
	--
	-- Return success
	log (self, "--> return 1");
	return 1
end

-- Rollback the changes under the self object
--
pulleyback_rollback = function (self)
	--
	-- Logging and checking
	log (self, "bind9slave.pulleyback_rollback (self)")
	--
	-- Revert any changes
	self.prepared = false
	self.txfail = false
	self.newzones = copyzones (self.oldzones)
	--
	-- Revert any dangling prepfile
	if not self.dryrun then
		os.remove (self.prepfile)
	end
	--
	-- Return quietly
	log (self, "--> return");
end

-- Collaboration is currently not supported
--
pulleyback_collaborate = function (self, other)
	--
	-- Logging and checking
	log (self, "bind9slave.pulleyback_collaborate (self, other)")
	--
	-- Return failure
	print ("Refusal: Uncertain if collaboration between Lua output drivers is possible")
	log (self, "--> return 0");
	return 0
end

