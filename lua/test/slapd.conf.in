# This file configures an insecure, world-writable, known-password
# LDAP server with the ARPA2 schemata. The rootdn is suitable for
# ARPA2 example data.

#
# See slapd.conf(5) for details on configuration options.
# This file should NOT be world readable.
#
include		@CMAKE_CURRENT_SOURCE_DIR@/schema/core.schema
include		@CMAKE_CURRENT_SOURCE_DIR@/schema/cosine.schema
include		@CMAKE_CURRENT_SOURCE_DIR@/schema/inetorgperson.schema

#
# ARPA2 specific schemas -- with .666. for experimental allocations
#
include		@CMAKE_CURRENT_SOURCE_DIR@/schema/arpa2-servicedir.schema
include		@CMAKE_CURRENT_SOURCE_DIR@/schema/arpa2-access.schema
include		@CMAKE_CURRENT_SOURCE_DIR@/schema/arpa2-reservoir.schema
include		@CMAKE_CURRENT_SOURCE_DIR@/schema/arpa2-lifecycle.schema
include		@CMAKE_CURRENT_SOURCE_DIR@/schema/arpa2-helm.schema

# Define global ACLs to disable default read access.

pidfile		@CMAKE_CURRENT_BINARY_DIR@/slapd.pid
argsfile	@CMAKE_CURRENT_BINARY_DIR@/slapd.args

# Load dynamic backend modules:
moduleload	back_mdb.la
moduleload	syncprov.la

# rootdn can always read and write EVERYTHING!

# Access Control; our demo accounts can do anything!
access	to *
	by * read
access	to *
	by * write

#######################################################################
# MDB database definitions
#######################################################################

database	mdb
maxsize		1073741824
suffix		ou=InternetWide
rootdn		"o=arpa2.net,ou=InternetWide"
rootpw		sekreet
directory	@SLAPD_DIR@
index		objectClass eq
monitoring	on

overlay syncprov
syncprov-checkpoint 100 10
syncprov-sessionlog 100
