# Pulley Backend for the ARPA2 Rules DB

> *The ARPA2 Rules DB holds information such as ARPA2 Groups
> and Access Control policy.  This information is stored in
> an efficient key/value database.  Information for the
> various uses can be configured with this generic plugin,
> which offers automation to mirror **a2rule** manual handling.*


General concepts of the ARPA2 Rules DB are:

  * **(Remote) User** is any party requesting Access Rights.
  * **Access Domain** is the serving domain name under which
    an access attempt is done.  It is an Internet domain name
    and may also be referred to as a secure realm.
  * **Access Type** is a general usage pattern, described
    by a well-known UUID value that may end up in fixed
    form in code bases specialising on it.
  * **Access Key** is an instance of the Access Type under
    a given Access Domain.  It takes the form of a UTF-8
    string, whose contents are defined by the Access Type.
  * **Access Rights** are flags of permitted actions, where
    the precise definitions are specific to the Access Type.

We are specific about local and remote users because we
allow Realm Crossover in our model; meaning, requests
can arrive from foreign realms that were authenticated,
but whose access is yet to be determined.


## LMDB structure

Keys indexing entries in LMDB are a function of

 1. Possibly an **encryption key** that is specific to either
    the Access Domain or its hosting provider.  Combined with
    the **Access Domain** to find a message digest.
 2. The former message digest is further specialised with the
    **Access Type** in binary 128-bit format.
 3. The LMDB lookup key is derived from the former message
    digest, the **Access Key** in UTF-8 form and the
    **Remote Selector**.

The Remote Selector is an abstraction from the ARPA2 Identity
of the Remote User.  It walks up from concrete to abstract in
a few steps, and invariably ends at the most abstract level,
`@.` for the catchall.  (TODO: Services would be `+@.` so
distinct from the most abstract user.)

The information found in LMDB is:

 1. Possibly encrypted with the lookup key
 2. An UTF-8 string spanning multiple lines
 3. Each line is individually evaluated from scratch
 4. Lines consist of words surrounded by spaces/tabs
 5. Words are `%rights`, `=keepvar`, `^callback`, `#disable`, `~selector`
 6. Note that the `~selector` is part of the key, so useless in LMDB


## LDAP structure

Information stored in LDAP follows a conceptual structure,
namely the Direction Information Tree.  We defined structures
for IndentityHub, Reservoir and more are likely to follow.
This information is not directly modelled for Access Control.

Individual objects hold the following attributes:

  * `object: accessControlledObject`
  * `accessType: 84283358-8ee3-444a-be2e-81e69f50b7fa`
  * `accessName: /some/identity/structure`
  * `accessDomain: example.com`
  * `accessRule: %r ~@. %cwr ~@example.com`
  * `accessRule: %acdwr ~admin@example.com`

Note that some of these parts may be implied by the context;
`associatedDomain` defines the Access Domain, but it may be
part of the DN leading to the object.  The `accessType` may
follow from the subtree in which the object is found.  As a
general rule will the Pulleyback plugin be provided with a
combination of Access Type, Access Key, Access Domain and
one Access Rule at a time.

One or more `accessRule` attributes each map to an ACL line
in LMDB, so they function like a set in both forms.

Any number of declarations may be summed up in the forms
`=keepvar`, `^callback` and `#disable`.  They are held
as provided, but do not necessarily produce anything.

The `%rights` declaration is stored, and replaces any prior
value.  Unlike in the LMDB form of rules does it not make
any callbacks to this plugin, so it yields no output.

The `~remote` declaration actually produces the preceding
declarations in LMDB, at the Remote Selector location
given in this declaration.  It may suppress `=keepvar`
that are not of influence and will summarise the `%rights`
to be the last one preceding the `~remote`.

After production for one `~remote`, the work continues.
Generally, the `%rights` and `=keepvars` carry over, but
the expensive form `^callback` does not unless it is
explicitly repeated; the form `#disable` will be quietly
ignored but may also be repeated; that has no consequences.

Any declaration after the last `~remote` will not end up
in the LMDB form of these rules.

We refer to this procedure as *turning the rules inside-out*
because this is pretty much what happens to the indexes.
The LDAP form is setup to bind Access Rules to the objects
that they describe, which is useful during configuration.
The LMDB form is setup for quickly iterating over a short
series of Remote Selector values.  The changes can be made
automatically however, and follow the rules given above.


## Pulleyback API

LMDB is a transactional database, but it lacks a "prepare
to commit" function, so two-phase commit is not available
through this interface.  Since there will not usually be
more databases involved this ought to be no problem.

The calls from Pulley into this backend retrieve their
information from the following sources:

  * *Remote User* comes from the LDAP rules' content and are
    passed in with those;
  * *Access Domain* is usually taken from the distinguishedName,
    so it is an input parameter.
  * *Access Type* is usually fixed for an application, it being
    a well-known identifier, so it is provided as a configuration
    string containing the UUID.
  * *Access Key* is derived from the application.  Pulleyback
    assumes that it can be taken literally from LDAP structures,
    such as attributes.
  * *Access Rights* come from the LDAP rules' content and are
    passed in with those.

So, a driver line could look like:

```
dom,xskey,rule -> ("ca71cfad-28d1-40cc-840b-924540b195eb")
```

**TODO:** Set the trunk number.  And can we set a `#comment` string in each record?

This assumes:

  * `ca71cfad-28d1-40cc-840b-924540b195eb` is the Access Type.
  * `dom` is a domain name in UTF-8 notation.  We used `associatedDomain` before,
    but for reasons of internationalisation may have to change to `accessDomain`
    instead.
  * `xskey` is an access key, subject to the specification for the given Access Type.
  * `rule` is a single `accessRule` attribute, specifying `%rights` and `=keepvars`
    in like with the given Access Type and `~remote` to indicate who receives the
    specified warmth of welcome.

A minimally complete (TODO: Untested) PulleyScript would resemble

```
Uid~xskey,accessDomain~dom,ou="IdentityHub",o="arpa2.org",ou="InternetWide" <- world
dom,xskey,rule -> ("ca71cfad-28d1-40cc-840b-924540b195eb")
```

