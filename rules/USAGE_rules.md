# Using the Pulley Backend for the ARPA2 Rules DB

<!-- SPDX-FileCopyrightText: 2020 Adriaan de Groot <groot@kde.org>
  -- SPDX-License-Identifier: BSD-2-Clause
  !>

## Examples

### Example Data

The sample data in the ARPA2 DIT (for instance, from the ARPA2 LDAP Docker
example) lives under the **base DN** `o=arpa2.net,ou=InternetWide`.
To check that there is such data available, use *ldapsearch* as follows:

```
	ldapsearch -h db -b "o=arpa2.net,ou=InternetWide"
```

(Here, `-h db` specifies the hostname of the LDAP server -- update it
to your local situation; you may also need `-p port` to set a port number,
if the LDAP server is running in a Docker container)

### Verify Plugin

To check that the plugin has been built and successfully installed,
use the *pulleyscript* tool (from Steamworks) to try to load the plugin:

```
	pulleyscript rules
```

or, to load the plugin from a build/test directory,

```
	pulleyscript -L test/dir rules
```

The tool outputs a lot of debugging information. If it is **un**successful,
you will see a message like "Could not load plugin acl".
