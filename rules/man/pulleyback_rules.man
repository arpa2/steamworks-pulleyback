.TH PULLEYBACK_RULES 8 "July 2021" "ARPA2.net" "System Management Commands"
.SH NAME
pulleyback_rules \- Pulley backend to subscribe an ARPA2 Rules DB to LDAP
.SH SYNOPSIS
.IB domain , name , rule " ->"
.B rules (accessType="\fIuuid\fB"\fR
.RS
                  [\fB,dbPrefix="#\fIword\fB"\fR]
.br
                  [\fB,dbTrunk="\fItrunkid\fB"\fR]\fB)
.RE
.PP
.SH DESCRIPTION
When used as a backend to Steamworks Pulley (REFTODO),
this shared object updates an ARPA2 Rules DB.  This may be
used as an automated counterpart to manual management of
the Rules DB with
.BR a2rule (8).
Pulley subscribes to one or more LDAP databases to process
updates as soon as possible.  In doing so, it may mix and match
elements from LDAP.  The synopsis presents the output driver line
in a PulleyScript.
.SH CONFIGURATION
Environment variables can be used to influence general
ARPA2 Rules DB processing.  See
.BR a2rule (8)
for details about
.BR ARPA2_RULES_DIR .
.PP
The following configuration parameters may be set in
an output driver line in a PulleyScript:
.TP
.BI accessType=" uuid """
specifies the Access Type in textual \fIuuid\fR notation.
This indicates the semantics of the application, without
resorting to protocol or application specifics.  It is
also used to find the appropriate ruleset in the Rules DB.
.TP
.BI dbPrefix="# word """
specifies the distinguishing \fB#\fIword\fR to specify in
each rule managed by this driver.  It defaults to \fB#ply\fR,
short for Pulley.  Since
.BR a2rule (1)
defaults to \fB#a2xs\fR, the tools deliberately do not write
to each other's rules.  Applications do not usually discriminate
between the origin of a rule.  This enables manual overrides
on top of automated management, with minimal interference.
.TP
.BI dbTrunk=" trunkid """
specifies the numeric \fItrunkid\fR that marks a source of
rules for purposes of batch processing.  A trunk may be used
to identify an uplink in a scheme devised by a local network
administrator.  In case of problems with a trunk, the
\fItrunkid\fR can help to remove problems in a bulk operation
without jeapourdising the rest of the Rules DB.
.TP
.B dbSecret
is not supported, because its use is questionable and
its risks are not.
Future use of environment variables is more likely,
especially the same \fBARPA2_DOMAINKEY_\fIdomain\fR
names that are also used by
.BR a2rule (8).
.SH PULLEY INPUT VARIABLES
Pulley matches variables to attribute values and to the
distinguished names of LDAP entries.  It equates and
constrains values, and delivers tuples of variables
into output drivers.  In the case of
.BR pulleyback_rules (8),
the following variables must be provided:
.TP
.I domain
is the Access Domain, so the domain name under which a
new rule must be provded.
.TP
.I name
is the Access Name, so the text string used to identify
a managed resource.  The precise grammar supported here
varies with the Access Type, but generally aims for a
concise and canonical string for each identifiable resource.
.TP
.I rule
is the Access Rule, so the single rule to be managed
as part of the Rules DB.  The grammar is basically the
general grammar for Policy Rules, but its interpretation
varies with the Access Type.  It may include Selectors
that indicate where in the Rules DB the \fIrule\fR is to
be added or removed.
.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the
InternetWide Architecture and its ARPA2 SteamWorks project.
.SH "REPORTING BUGS"
For any discussion, including about bugs, please use the
.IR https://gitlab.com/arpa2/steamworks/ " or " https://gitlab.com/arpa2/steamworks-pulleyback/
project pages for ARPA2 SteamWorks and Pulley backends.
We use the related issue tracker
to track bugs and feature requests.
.SH COPYRIGHT
.PP
Copyright \(co 2021 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turn receives
funding from various sources.
fees for .nl domain names.  Parts of this work were funded by
by the Dutch Ministry of Economic Affairs,
by NLnet, a public interest fund geared towards open source excellence
and by the NGI Pointer fund of the European Union.
.\" .SH "SEE ALSO"
.\" ARPA2 Common is documented on
.\" .IR http://common.arpa2.net/
.\" and its code is on
.\" .IR https://gitlab.com/arpa2/arpa2common/
