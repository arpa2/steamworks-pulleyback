#!/usr/bin/env python3
#
# Retrieve keepvars, callbacks, comments keepvars


import sys
import pickle
import uuid


# We will not hash the database keys, but turn them into strings
hash3levels = str


if len (sys.argv) != 6:
	sys.stderr.write ('Usage: %s file.kv domain.name accessType accessName [visitor]@[remote].domain\n' % (sys.argv [0],))
	sys.exit (1)
pjar = sys.argv [1]
dom = sys.argv [2]
xstp = str (uuid.UUID (sys.argv [3]))
xsky = sys.argv [4]
rsel = sys.argv [5]

# Read "kv" from the pickle jar
if pjar [-3:] != '.kv':
	pjar = pjar + '.kv'
fh = open (pjar, 'rb')
kv = pickle.load (fh)
fh.close ()
#DEBUG# print ('kv = %r' % (kv,))

# Form the key
key1 = ['dbsecret', dom]
print ('key1 =   %r' % (key1,))
key2 = [key1, xstp]
print ('key2 =  %r' % (key2,))
key3 = [key2, xsky, rsel]
print ('key3 = %r' % (key3,))
try:
	rval = kv [hash3levels (key3)]
	print (' +---> %r' % (rval,))
except KeyError as ke:
	sys.stderr.write ('No such entry: %r\n' % (ke,))
	sys.stderr.write ('Try to abstract %r:\n' % (rsel,))
	for k in kv:
		sys.stderr.write (' - %s\n' % (k,))


