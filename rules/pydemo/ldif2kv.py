#!/usr/bin/env python3
#
# Map LDIF to the key/value form used by LMDB

import sys
import re
import pickle

import ldif

dn2dom = re.compile ('^.*, *associatedDomain=([^,]*),.*$')
rulespc = re.compile ('[ \t]+')


# We will not hash the database keys, but turn them into strings
hash3levels = str


# Given an accessRule field, produce (remote,words) pairs
def rule2pairs (atval):
	if '\n' in atval:
		sys.stderr.write ('You should not use newlines in accessRule attributes; simply define more of them\n')
		sys.exit (1)
	#
	# Retrieve the rule's words
	#DEBUG# print ('ATVAL = %r' % (atval,))
	words = rulespc.split (atval)
	#DEBUG# print ('WORDS = %r' % (words,))
	#
	# Collect information about words
	retval = [ ]
	outw = [ ]
	rights = '%V'
	goners = [ ]
	for inw in words:
		goners.append (inw)
		if inw == '':
			# Just a parsing hickup
			continue
		elif inw [:1] in ['#', '^', '=']:
			if inw [:1] == '=':
				# Drop any previous keepvar with the same name
				outw = [ ow for ow in outw if ow [:2] != inw [:2] ]
			outw.append (inw)
		elif inw [:1] == '%':
			rights = inw
		elif inw [:1] == '~':
			retval.append ( (inw[1:],outw + [rights]) )
			outw = [ ow for ow in outw if ow [:1] != '^' ]
			goners = [ ]
		else:
			sys.stderr.write ('Unknown accessRule word: %s' % (inw,))
			sys.exit (1)
	if len (goners) > 0:
		sys.stderr.write ('Lost tail: %r\n' % (goners,))
	return retval

# Process LDIF iterator into a dictionary
def ldif2kv (ldif):
	kv = { }
	for (dn,attrs) in ldif:
		#DEBUG# print ('LDIF> %r' % (dn,))
		#DEBUG# # print (attrs)
		#DEBUG# for (atnm,atvals) in attrs.items ():
		#DEBUG# 	print (' - \"%s\":' % (atnm,))
		#DEBUG# 	for atval in atvals:
		#DEBUG# 		print ('\t\"%s\"' % atval)
		# Produce a key
		try:
			dom = dn2dom.match (dn).group (1)
			key1 = ['dbsecret', dom]
			print ('key1 =   %r' % (key1,))
			xstp = attrs ['accessType'] [0]
			key2 = [key1, xstp]
			print ('key2 =  %r' % (key2,))
			xsky = attrs ['accessName'] [0]
			for xsrl in attrs ['accessRule']:
				for (rsel,rval) in rule2pairs (xsrl):
					#DEBUG# print ('rsel -> rval = %r -> %r' % (rsel,rval))
					key3 = [key2, xsky, rsel]
					print ('key3 = %r' % (key3,))
					print (' +---> %r' % (rval,))
					kv [hash3levels (key3)] = rval
		except Exception as e:
			print ('Exception:', e)
			raise
			continue
	return kv


# Check args
if len (sys.argv) < 2:
	sys.stderr.write ('Usage: %s file.ldif [...]\n' % sys.argv [0])
	sys.exit (1)

# Iterate files
for fn in sys.argv [1:]:
	if fn [-5:] != '.ldif':
		fn = fn + '.ldif'
	fh = open (fn, 'rb')
	fp = ldif.LDIFParser (fh)
	kv = ldif2kv (fp.parse ())
	fh.close ()
	#DEBUG# print ('%r' % kv)
	fn = fn [:-5] + '.kv'
	fh = open (fn, 'wb')
	pickle.dump (kv, fh)
	fh.close ()
	print ('Written %s' % (fn,))

