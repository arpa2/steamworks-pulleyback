/*
SPDX-FileCopyrightText: Copyright 2020-2021 Rick van Rein <rick@openfortress.nl>
SPDX-FileCopyrightText: 2019-2021 InternetWide.org and the ARPA2.net project
SPDX-FileCopyrightText: 2019 Adriaan de Groot <groot@kde.org>
SPDX-License-Identifier: BSD-2-Clause
*/

#include <steamworks/pulleyback.h>

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include <errno.h>

#include <uuid/uuid.h>

#include <arpa2/access.h>
#include <arpa2/rules_db.h>



static const char logger[] = "steamworks.pulleyback.rules";


/* Handle structure.  Comes with the UUID of the Access Type, the
 * Rules DB and whether it is open (for a read/write transaction).
 */
typedef struct {
	uuid_t xstype;
	struct rules_db ruledb;
	bool txopen;
	bool txfail;
	const char *dbprefix;
	unsigned dbprefixlen;
	uint32_t dbtrunk;
} handle_t;


/* Fork structure.  These are the three bits of information that
 * should be provided: Access Domain, Access Name, Access Rule.
 */
typedef struct fork {
	der_t domain;
	der_t xsname;
	der_t xsrule;
} fork_t;


/* Parse the pointer and length from a DER header.
 * Return success as true, failure as false.
 */
static bool parse_der (uint8_t *der, char **ptr, size_t *len) {
	der++;
	if ((*der & 0x80) != 0x00) {
		int lenlen = (*der & 0x7f);
		if ((lenlen < 1) || (lenlen > 2)) {
			return false;
		}
		*len = der [0];
		if (lenlen == 2) {
			*len = ((*len) << 8) | der [1];
			*ptr = (char *) (der + 2);
		} else {
			*ptr = (char *) (der + 1);
		}
	} else {
		*len = *der;
		*ptr = (char *) (der + 1);
	}
	return true;
}

static bool have_transaction (handle_t *pbh) {
	bool ok = true;
	if (!pbh->txopen) {
		memset (&pbh->ruledb, 0, sizeof (pbh->ruledb));
		ok = ok && rules_dbopen (&pbh->ruledb, false, pbh->dbtrunk);
		pbh->txopen = ok;
		pbh->txfail = !ok;
	}
	return ok;
}


/* Find a configuration parameter.  Tease out the start and length of the value.
 * The assumption is that the confprefix ends in equals sign and quote;
 * the routine will test for the matching closing quite.
 */
static bool find_config (int argc, char **argv, char *confprefix, char **confptr, unsigned *conflen) {
	int confprefixlen = strlen (confprefix);
	for (int argi=0; argi < argc; argi++) {
		if (memcmp (argv [argi], confprefix, confprefixlen) != 0) {
			continue;
		}
		unsigned arglen = strlen (argv [argi]);
		if (argv [argi] [arglen-1] != confprefix [confprefixlen-1]) {
			continue;
		}
		*confptr = argv [argi] + confprefixlen;
		*conflen = arglen - confprefixlen - 1;
		return true;
	}
	return false;
}


/* Open a Pulleyback handle, to use throughout the script run.
 * The database will not be opened, because that involves
 * write locking the entire database.  Instead, a lazy open
 * will be performed when needed.  If not explicitly closed
 * with rollback or commit, the transaction will be closed
 * during pulleyback_close().
 */
void *pulleyback_open(int argc, char **argv, int varc)
{
	//
	// Debug logging
	write_logger (logger, "ARPA2 Rules DB Backend open request");
	write_logger (logger, " .. expecting 3 attributes: Access Domain, Access Name, Access Rule");
	write_logger (logger, " .. required config-params: accessType=\"<UUID>\"");
	write_logger (logger, " .. advisory config-params: dbTrunk=\"<NUMBER>\" -- defaults to 0");
	write_logger (logger, " .. optional config-params: dbPrefix=\"#<WORD>\" -- defaults to \"#ply\"");
	//
	// Ensure that we will get the desired 3 attributes
	if (varc != 3) {
		if (varc >= 0) {
			write_logger (logger, "ARPA2 Rules DB backend fails to open: need 3 attributes");
			errno = EINVAL;
			return NULL;
		} else {
			write_logger (logger, "ARPA2 Rules DB backend assumes test mode; do not add or delete tuples from forks");
		}
	}
	//
	// Count the number of arguments found
	int argf = 0;
	//
	// Find the Access Type UUID
	char *xstype;
	unsigned xstypelen;
	if (find_config (argc, argv, "accessType=\"", &xstype, &xstypelen)) {
		argf++;
	} else {
		write_logger (logger, "ARPA2 Rules DB backend requires the accessType config-parameter");
		if (varc >= 0) {
			errno = EINVAL;
			return NULL;
		}
	}
	//
	// Find the Database Prefix (optional)
	char *dbprefix = "#ply";
	unsigned dbprefixlen = 4;
	if (find_config (argc, argv, "dbPrefix=\"", &dbprefix, &dbprefixlen)) {
		argf++;
	}
	//
	// Find the Database Trunk (optional)
	char *dbtrunk = "0";
	unsigned dbtrunklen = 1;
	if (find_config (argc, argv, "dbTrunk=\"", &dbtrunk, &dbtrunklen)) {
		argf++;
	}
	//
	// All config-parameters should now be parsed
	if ((argf != argc) && (varc >= 0)) {
		write_logger (logger, "ARPA2 Rules DB backend did not recognise all config-parameters");
		errno = EINVAL;
		return NULL;
	}
	//
	// Allocate handle memory
	handle_t *pbh = calloc (sizeof (handle_t), 1);
	if (pbh == NULL) {
		// Not even // write_logger (logger, "ARPA2 Rules DB backend ran out of memory");
		return NULL;
	}
	//
	// Parse and store accessType="...UUID..."
	bool bad_uuid = false;
	bad_uuid = bad_uuid || (xstypelen != 36);
	char cpuuid [36 + 1];
	if (!bad_uuid) {
		memcpy (cpuuid, xstype, 36);
		cpuuid [36] = '\0';
	}
	bad_uuid = bad_uuid || (uuid_parse (cpuuid, pbh->xstype) != 0);
	bad_uuid = bad_uuid || (uuid_is_null (pbh->xstype) != 0);
	if (bad_uuid) {
		write_logger (logger, "ARPA2 Rules DB backend could not parse the accessType UUID");
		errno = EINVAL;
		free (pbh);
		return NULL;
	}
	//
	// Parse and store advisory dbTrunk="..." (in an uint32_t)
	char *dbtrunk_end;
	unsigned long trunk_ul = strtoul (dbtrunk, &dbtrunk_end, 0);
	pbh->dbtrunk = trunk_ul;
	if ((dbtrunk_end != dbtrunk + dbtrunklen) || (trunk_ul != (unsigned long) pbh->dbtrunk)) {
		write_logger (logger, "ARPA2 Rules DB backend could not parse the dbTrunk number");
		errno = EINVAL;
		free (pbh);
		return NULL;
	}
	//
	// Store optional dbPrefix="#..." or use the default "#ply"
	pbh->dbprefix    = dbprefix   ;
	pbh->dbprefixlen = dbprefixlen;
	//
	// Return the configured setup
	write_logger (logger, "ARPA2 Rules DB backend opened successfully");
	return pbh;
}

/* Close a pulleyback handle, which may involve tearing down a
 * transaction if one is still running.
 */
void pulleyback_close (void *handle)
{
	handle_t *pbh = handle;
	if (pbh == NULL) {
		return;
	}
	write_logger (logger, "ARPA2 Rules DB backend closes");
	if (pbh->txopen) {
		rules_dbclose (&pbh->ruledb);
		pbh->txopen = false;
	}
	free (pbh);
}

static int rule_change (handle_t *pbh, fork_t *forkdata, changerules_what whattodo)
{
	bool ok = true;
	//
	// Parse single DER attributes into ptr,len values
	char *domain = NULL;
	char *xsname = NULL;
	char *xsrule = NULL;
	size_t domainlen = 0;
	size_t xsnamelen = 0;
	size_t xsrulelen = 0;
	ok = ok && parse_der (forkdata->domain, &domain, &domainlen);
	ok = ok && parse_der (forkdata->xsname, &xsname, &xsnamelen);
	ok = ok && parse_der (forkdata->xsrule, &xsrule, &xsrulelen);
	//
	// Make UTF-8 strings (safe and fast, even if parse_der() did not run)
	char domainstr [domainlen + 1];
	char xsnamestr [xsnamelen + 1];
	char xsrulestr [pbh->dbprefixlen + 1 + xsrulelen + 1];
	memcpy (domainstr, domain, domainlen);
	memcpy (xsnamestr, xsname, xsnamelen);
	memcpy (xsrulestr, pbh->dbprefix, pbh->dbprefixlen);
	xsrulestr [pbh->dbprefixlen] = ' ';
	memcpy (xsrulestr + pbh->dbprefixlen + 1, xsrule, xsrulelen);
	domainstr [sizeof (domainstr) - 1] = '\0';
	xsnamestr [sizeof (xsnamestr) - 1] = '\0';
	xsrulestr [sizeof (xsrulestr) - 1] = '\0';
	//
	// Report attribute data to logging
	if (ok) {
		write_logger (logger, "ARPA2 Rules DB backend changes Access Domain, Name and Rule attributes:");
		write_logger (logger, domainstr);
		write_logger (logger, xsnamestr);
		write_logger (logger, xsrulestr);
	} else {
		write_logger (logger, "ARPA2 Rules DB backend could not parse DER attributes for Access Domain, Name and Rule");
	}
	//
	// Prepare the Rules DB key with domain and service, but not yet selector iteration
	rules_dbkey domkey, svckey;
	ok = ok && rules_dbkey_domain   (domkey,                          (const uint8_t *) "", 0, domainstr);
	ok = ok && rules_dbkey_service  (svckey, domkey, sizeof (domkey), pbh->xstype);
	//
	// If we don't have a transaction yet, get one now
	if (ok && !have_transaction (pbh)) {
		ok = false;
		write_logger (logger, "ARPA2 Rules DB could not open a transaction");
	}
	//
	// Do not continue if this transaction is already marked as failed
	if (ok && pbh->txfail) {
		ok = false;
		write_logger (logger, "ARPA2 Rules DB failed before on this transaction");
	}
	//
	// Add or Del the prepared rule -- with no fixed remote selector
	if (ok && !rules_edit_generic (svckey, sizeof (svckey),
					xsnamestr,
					xsrulestr, sizeof (xsrulestr),
					whattodo,
					NULL,
					&pbh->ruledb)) {
		ok = false;
		write_logger (logger, "ARPA2 Rules DB backend failed to install the change from LDAP");
	}
	//
	// Take note of any failure
	if (!ok) {
		pbh->txfail = true;
	}
	//
	// Return success or failure
	return (ok ? 1 : 0);
}

int pulleyback_add (void *handle, der_t *forkdata)
{
	//
	// Report the callback
	char ibuf[128];
	snprintf (ibuf, sizeof(ibuf), "ARPA2 Rules DB backend add %p data %p", handle, (void*)forkdata);
	write_logger (logger, ibuf);
	//
	// Call the generic routine
	return rule_change ((handle_t *) handle, (fork_t *) forkdata, do_add);
}

int pulleyback_del (void *handle, der_t *forkdata)
{
	//
	// Report the callback
	char ibuf[128];
	snprintf (ibuf, sizeof(ibuf), "ARPA2 Rules DB backend del %p data %p", handle, (void*)forkdata);
	write_logger (logger, ibuf);
	//
	// Call the generic routine
	return rule_change ((handle_t *) handle, (fork_t *) forkdata, do_del);
}

//
// OPTIONAL EXTENSION, NOT POSSIBLE ON LMDB:
// int pulleyback_prepare (void *handle);
//

int pulleyback_commit (void *handle)
{
	handle_t *pbh = handle;
	bool ok = true;
	//
	// Test if a transaction is currently open
	if (!pbh->txopen) {
		/* nothing to do, a silent triumph */
		write_logger (logger, "Transaction is a trivial succes without changes");
		return 1;
	}
	//
	// Test if the transaction has failed
	if (pbh->txfail) {
		/* Refuse to commit the transaction */
		write_logger (logger, "Transaction failed before");
		/* Just set non-ok, but continue to close the database */
		ok = false;
	}
	//
	// Commit the transaction.  This implicitly closes the database.
	ok = ok && rules_dbcommit (&pbh->ruledb);
	if (!ok) {
		/* Ignore output.  This is a mere politeness call. */
		rules_dbclose (&pbh->ruledb);
	}
	//
	// Return success or failure, but always have a closed transaction
	write_logger (logger, ok ? "Transaction succes" : "Transaction failure");
	pbh->txopen = false;
	return (ok ? 1 : 0);
}

void pulleyback_rollback (void *handle)
{
	//
	// Report the callback
	write_logger (logger, "Transaction rollback");
	//
	// Initialise
	handle_t *pbh = handle;
	bool ok = true;
	//
	// Test if a transaction is currently open
	if (!pbh->txopen) {
		/* nothing to roll back, which is a trivial success */
		return;
	}
	//
	// Commit the transaction.  This implicitly closes the database.
	ok = ok && rules_dbrollback (&pbh->ruledb);
	if (!ok) {
		/* Ignore output.  This is a mere politeness call. */
		rules_dbclose (&pbh->ruledb);
	}
	//
	// Break on failure, but be sure to have a closed transaction
	pbh->txopen = false;
	assert (ok);
}

int pulleyback_collaborate (void *pbh1, void *pbh2) {
	(void) pbh1;
	(void) pbh2;
	/* Not considered possible on LMDB, so fail */
	return 0;
}

int pulleyback_reset (void *pbh) {
	(void) pbh;
	//TODO// Erase all keys (with the given TrunkID) within the transaction
	return 0;
}

